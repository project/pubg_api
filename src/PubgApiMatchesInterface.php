<?php

namespace Drupal\pubg_api;

/**
 * Pubg Api Matches Interface.
 *
 * @see https://documentation.playbattlegrounds.com/en/matches.html
 */
interface PubgApiMatchesInterface {

  /**
   * Get a single match.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $match_id
   *   The match id to search for.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getSingleMatch(string $shard, string $match_id);

}
