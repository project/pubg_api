<?php

namespace Drupal\pubg_api;

/**
 * PUBG API Data.
 */
class Data {

  const PUBG_API_BASE_URL = 'https://api.pubg.com/shards';

  const PUBG_GET_API_KEY_URL = 'https://developer.pubg.com';

  const PUBG_SHARDS = [
    'kakao',
    'psn',
    'stadia',
    'steam',
    'xbox',
  ];

}
