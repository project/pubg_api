<?php

namespace Drupal\pubg_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Pubg Api Matches methods.
 */
final class PubgApiMatches extends PubgApiBase implements PubgApiMatchesInterface {

  /**
   * API endpoint base.
   *
   * @var string
   */
  protected $apiEndpointBase;

  /**
   * PubgApiMatches constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A guzzle http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    parent::__construct($http_client, $config_factory, $messenger);
    $this->apiEndpointBase = "matches";
  }

  /**
   * {@inheritdoc}
   */
  public function getSingleMatch(string $shard, string $match_id) {
    $api_endpoint = "{$this->apiEndpointBase}/{$match_id}";
    $endpoint_options = [];

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

}
