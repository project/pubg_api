<?php

namespace Drupal\pubg_api;

/**
 * Pubg Api Matches interface.
 *
 * @see https://documentation.playbattlegrounds.com/en/players.html
 */
interface PubgApiPlayersInterface {

  /**
   * Get a collection of players.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_ids
   *   An optional comma separated list of players PUBG id.
   * @param string $player_names
   *   An optional comma separated list of players names.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getPlayers(string $shard, string $player_ids = '', string $player_names = '');

  /**
   * Get a collection of players.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_id
   *   A player PUBG id.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getSinglePlayer(string $shard, string $player_id);

  /**
   * Get lifetime stats for a single player.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_id
   *   A player PUBG id.
   * @param bool $gamepad
   *   Filters for gamepad stats for stadia players.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getPlayerLifetimeStats(string $shard, string $player_id, bool $gamepad = true);

  /**
   * Get weapon mastery information for a single player.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_id
   *   A player PUBG id.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getPlayerWeaponMastery(string $shard, string $player_id);

  /**
   * Get survival mastery information for a single player.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_id
   *   A player PUBG id.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getPlayerSurvivalMastery(string $shard, string $player_id);

  /**
   * Get ranked stats for a single player in a given season.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_id
   *   The PUBG player account id.
   * @param string $season_id
   *   The season id.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getPlayerRankedStats(string $shard, string $player_id, string $season_id);

  /**
   * Get season informations for a single player in a given season.
   *
   * @param string $shard
   *   A valid PUBG shard.
   * @param string $player_id
   *   The PUBG player account id.
   * @param string $season_id
   *   The season id.
   *
   * @return array|string
   *   The API call response or an error message.
   */
  public function getPlayerSeasonInfos(string $shard, string $player_id, string $season_id);

}
