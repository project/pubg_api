<?php

namespace Drupal\pubg_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Pubg Api Matches methods.
 */
final class PubgApiPlayers extends PubgApiBase implements PubgApiPlayersInterface {

  /**
   * PubgApiPlayers constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A guzzle http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    parent::__construct($http_client, $config_factory, $messenger);
    $this->apiEndpointBase = "players";
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayers(string $shard, string $player_ids = '', string $player_names = '') {
    $endpoint_options = [];

    if (!empty($player_ids)) {
      $endpoint_options['query']['filter[playerIds]'] = $player_ids;
    }

    if (!empty($player_names)) {
      $endpoint_options['query']['filter[playerNames]'] = $player_names;
    }

    $response = $this->getResponse($shard, $this->apiEndpointBase, $endpoint_options);

    return $response ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSinglePlayer(string $shard, string $player_id) {
    $api_endpoint = "{$this->apiEndpointBase}/{$player_id}";
    $endpoint_options = [];

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerLifetimeStats(string $shard, string $player_id, bool $gamepad = true) {
    $api_endpoint = "{$this->apiEndpointBase}/{$player_id}/seasons/lifetime";
    $endpoint_options = [];

    if ($shard == 'stadia') {
      $endpoint_options['query']['filter[gamepad]'] = $gamepad;
    }

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerWeaponMastery(string $shard, string $player_id) {
    $api_endpoint = "{$this->apiEndpointBase}/{$player_id}/weapon_mastery";
    $endpoint_options = [];

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerSurvivalMastery(string $shard, string $player_id) {
    $api_endpoint = "{$this->apiEndpointBase}/{$player_id}/survival_mastery";
    $endpoint_options = [];

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerRankedStats(string $shard, string $player_id, string $season_id) {
    $api_endpoint = "{$this->apiEndpointBase}/{$player_id}/seasons/{$season_id}/ranked";
    $endpoint_options = [];

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerSeasonInfos(string $shard, string $player_id, string $season_id) {
    $api_endpoint = "{$this->apiEndpointBase}/{$player_id}/seasons/{$season_id}";
    $endpoint_options = [];

    $response = $this->getResponse($shard, $api_endpoint, $endpoint_options);

    return $response ?? [];
  }

}
