<?php

namespace Drupal\pubg_api_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pubg_api\PubgApiPlayersInterface;
use Drupal\pubg_api\PubgApiSeasonsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * PUBG API Players example controller.
 */
class PlayersController extends ControllerBase {

  /**
   * PUBG API Players service.
   *
   * @var \Drupal\pubg_api\PubgApiPlayersInterface
   */
  protected $pubgApiPlayers;

  /**
   * PUBG API Seasons service.
   *
   * @var \Drupal\pubg_api\PubgApiSeasonsInterface
   */
  protected $pubgApiSeasons;

  /**
   * PUBG API Players example constructor.
   *
   * {@inheritdoc}
   *
   * @param \Drupal\pubg_api\PubgApiPlayersInterface $pubg_api_players
   *   The PUBG API Players service.
   * @param \Drupal\pubg_api\PubgApiSeasonsInterface $pubg_api_seasons
   *   The PUBG API Seasons service.
   */
  public function __construct(
    PubgApiPlayersInterface $pubg_api_players,
    PubgApiSeasonsInterface $pubg_api_seasons
  ) {
    $this->pubgApiPlayers = $pubg_api_players;
    $this->pubgApiSeasons = $pubg_api_seasons;
  }

  /**
   * Plugin dependencies injection.
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pubg_api.players'),
      $container->get('pubg_api.seasons')
    );
  }

  /**
   * PUBG API Players Example build.
   *
   * @return array
   *   A renderable array.
   */
  public function build() {
    $build = [];

    kint($this->pubgApiPlayers->getPlayers(
      'steam',
      '',
      'ReKoNe75,infini'
    ), 'Get 2 players by name on steam shard');

    kint($this->pubgApiPlayers->getPlayers(
      'steam',
      'account.3464d685979b436a98490c7220790e34,account.41e6a168e25d41e28fd35467a29b1814'
    ), 'Get 2 players by id on steam shard');

    kint($this->pubgApiPlayers->getSinglePlayer(
      'steam',
      'account.3464d685979b436a98490c7220790e34'
    ), 'Get single player by id');

    kint($this->pubgApiPlayers->getPlayerLifetimeStats(
      'steam',
      'account.3464d685979b436a98490c7220790e34'
    ), 'Get player lifetime stats');

    kint($this->pubgApiPlayers->getPlayerWeaponMastery(
      'steam',
      'account.3464d685979b436a98490c7220790e34'
    ), 'Get player weapon mastery');

    kint($this->pubgApiPlayers->getPlayerSurvivalMastery(
      'steam',
      'account.3464d685979b436a98490c7220790e34'
    ), 'Get player survival mastery');

    kint($this->pubgApiSeasons->getAllSeasons('steam'), 'Get all seasons');

    kint($this->pubgApiPlayers->getPlayerRankedStats(
      'steam',
      'account.3464d685979b436a98490c7220790e34',
      'division.bro.official.pc-2018-17',
    ), 'Get player season 14');

    $build[] = [
      '#markup' => "Beware, by default PUBG API calls are limited to 10 per minute. That page make 6 calls each time you reload it which can lead to <em>&laquo;429 Too Many Requests&raquo;</em> responses.",
    ];

    return $build;
  }

}
