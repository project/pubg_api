# PUBG API

## Introduction
**PUBG API** provides services to interact with PUBG using the [official PUBG Web API](https://documentation.pubg.com/en/introduction.html).

*Please note that only Matches and Players methods are supported actually.*

## Configuration
After the module has been enabled, access `/admin/config/services/pubg_api` and set your PUBG API key.

*The configuration form provides a link to the PUBG page where you can create or retrieve your PUBG API key.*

## How To use it

2 services provided:

- `pubg_api.matches`
  - `getSingleMatch(string $shard, string $match_id)`
- `pubg_api.players`
  - `getPlayers(string $shard, string $player_ids = '', string $player_names = '')`
  - `getSinglePlayer(string $shard, string $player_id)`
  - `getPlayerLifetimeStats(string $shard, string $player_id, bool $gamepad = true)`
  - `getPlayerWeaponMastery(string $shard, string $player_id)`
  - `getPlayerSurvivalMastery(string $shard, string $player_id)`

Supported shard values are:

- kakao
- psn
- stadia
- steam
- xbox

Have a look to the pubg_api_examples submodule to see how to use the services.
